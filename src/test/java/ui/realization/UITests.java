package ui.realization;

import cucumber.api.java.ru.Дано;
import cucumber.api.java.ru.Когда;
import cucumber.api.java.ru.Тогда;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import ui.pages.LoginPage;
import ui.pages.MainPage;

import java.util.ArrayList;
import java.util.List;

public class UITests{

    int timeout = 10;
    WebDriver driver = Hooks.driver;

    @Дано("^открываем главную страницу$")
    public void открываемГлавнуюСтраницу() {
        driver.navigate().to(LoginPage.BASE_URL);
    }

    @Дано("^вводим имя пользователя ([^\\\"]*) пароль ([^\\\"]*)$")
    public void вводимИмяПользователяИПароль(String arg1, String arg2) {
        driver.findElement(By.xpath(LoginPage.LOGIN_INPUT_XPATH)).sendKeys(arg1);
        driver.findElement(By.xpath(LoginPage.PASSWORD_INPUT_XPATH)).sendKeys(arg2);
    }

    @Когда("^нажимаем кнопку для входа$")
    public void нажимаемКнопкуДляВхода() {
        driver.findElement(By.xpath(LoginPage.SIGN_IN_BUTTON_XPATH)).click();
    }

    @Тогда("^появляется главная страница$")
    public void появляетсяГлавнаяСтраница() {
        Assert.assertEquals(driver.getCurrentUrl(), MainPage.BASE_URL);
    }

    @Когда("^открываем список игроков$")
    public void открываемСписокИгроков() {
        driver.findElement(By.xpath(MainPage.USERS_MENU_ITEM_XPATH)).click();
        driver.findElement(By.xpath(MainPage.PLAYERS_MENU_ITEM_XPATH)).click();
        (new WebDriverWait(driver, timeout))
            .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(MainPage.PLAYERS_TABLE_XPATH)));
    }

    @Тогда("^появляется список игроков")
    public void появляетсяСписокИгроков() {
        Assert.assertEquals(driver.getCurrentUrl(), MainPage.PLAYERS_URL);
        Assert.assertTrue(driver.findElement(By.xpath(MainPage.PLAYERS_TABLE_XPATH)).isDisplayed(),
                "players table is not visible");
    }

    @Когда("^кликаем по первому столбцу$")
    public void кликаемПоПервомуСтолбцу() {
        driver.findElement(By.xpath(MainPage.PLAYERS_TABLE_USERNAME_HEAD_XPATH)).click();
        (new WebDriverWait(driver, timeout))
            .until(ExpectedConditions.not(ExpectedConditions.attributeContains(By.xpath(MainPage.PLAYERS_GRID_XPATH),
                    "class","loading")));
    }

    @Тогда("^проверяем сортировку по возрастанию")
    public void проверяемСортировкуПоВозрастанию() {
        List<String> usernames = new ArrayList();
        driver.findElements(By.xpath(MainPage.PLAYERS_TABLE_USERNAME_VALUE_XPATH))
                .forEach(element -> usernames.add(element.getText()));
        for(int i=0;i<usernames.size();i++) {
            Assert.assertTrue(usernames.get(i).compareTo(usernames.get(i+1)) < 0,
                    ""+usernames.get(i) + " is lexically lower than "+ usernames.get(i+1));
        }
    }
}