package ui.pages;

public class MainPage {

    public static final String BASE_URL = "http://test-app.d6.dev.devcaz.com/configurator/dashboard/index";
    public static final String PLAYERS_URL = "http://test-app.d6.dev.devcaz.com/user/player/admin";

    public static final String USERS_MENU_ITEM_XPATH = "//ul[@id='nav']//li//span[text()='Users']";
    public static final String PLAYERS_MENU_ITEM_XPATH = "//ul[@id='s-menu-users']//a[contains(text(),'Players')]";
    public static final String PLAYERS_GRID_XPATH = "//div[@id='payment-system-transaction-grid']";
    public static final String PLAYERS_TABLE_XPATH = "//div[@id='payment-system-transaction-grid']//table";
    public static final String PLAYERS_TABLE_USERNAME_HEAD_XPATH = "//div[@id='payment-system-transaction-grid']//table//th//a[text()='Username']";
    public static final String PLAYERS_TABLE_USERNAME_VALUE_XPATH = "//div[@id='payment-system-transaction-grid']//table//td[2]//a";

}
