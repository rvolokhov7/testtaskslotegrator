package ui.pages;

public class LoginPage {

    public static final String BASE_URL = "http://test-app.d6.dev.devcaz.com/admin/login";
    public static final String LOGIN_INPUT_XPATH = "//input[@id='UserLogin_username']";
    public static final String PASSWORD_INPUT_XPATH = "//input[@id='UserLogin_password']";
    public static final String SIGN_IN_BUTTON_XPATH = "//input[@value='Sign in']";

}
