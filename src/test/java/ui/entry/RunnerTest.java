package ui.entry;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/features",
        glue = "ui/realization",
        tags = "@all",
        snippets = SnippetType.CAMELCASE
)
public class RunnerTest {
}