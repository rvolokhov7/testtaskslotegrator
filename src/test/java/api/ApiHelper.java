package api;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.RequestSpecification;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.testng.annotations.BeforeMethod;

import java.util.Base64;

public class ApiHelper {

    /*
    /* CONSTANTS *
    */
    public static final String BASE_URL = "http://test-api.d6.dev.devcaz.com";
    public static final String OAUTH_URL_PART = "/v2/oauth2/token";
    public static final String PLAYERS_URL_PART = "/v2/players";
    public static final String SPECIFIC_PLAYER_URL_PART = "/v2/players/%s";

    /*
    /* METHODS *
    */

    @BeforeMethod
    public static void test() {
        Log.nullifySteps();
    }

    static String generatePostfix() {
        return RandomStringUtils.randomAlphabetic(10);
    }

    public static String getGuestToken(String basicToken) {
        basicToken = basicToken + ":";
        String encodedToken = Base64.getEncoder().encodeToString(basicToken.getBytes());
        RequestSpecification request = RestAssured.given();
        JSONObject requestJson = new JSONObject();
        requestJson.put("grant_type", "client_credentials");
        requestJson.put("scope", "guest:default");
        Log.info("Performing POST request to " + BASE_URL + OAUTH_URL_PART);
        return request.contentType("application/json")
                .header("Authorization", "Basic " + encodedToken)
                .body(requestJson.toString())
                .post(BASE_URL + OAUTH_URL_PART)
                .then().assertThat().statusCode(HttpStatus.SC_OK)
                .extract().asString();
    }

    public static String newPlayerRegistration(String bearerToken, String userName, String password, String email, String name, String surname) {

        RequestSpecification request = RestAssured.given();
        JSONObject requestJson = new JSONObject();
        requestJson.put("username", userName)
                .put("password_change", Base64.getEncoder().encodeToString(password.getBytes()))
                .put("password_repeat", Base64.getEncoder().encodeToString(password.getBytes()))
                .put("email", email)
                .put("name", name)
                .put("surname", surname);
        Log.info("Performing POST request to " + BASE_URL + PLAYERS_URL_PART);
        String response = request.contentType("application/json")
                .header("Authorization", "Bearer " + bearerToken)
                .body(requestJson.toString())
                .post(BASE_URL + PLAYERS_URL_PART)
                .then()//.assertThat().statusCode(HttpStatus.SC_CREATED)
                .extract().asString();
        return response;

    }

    public static String authorization(String basicToken, String login, String password) {
        basicToken = basicToken + ":";
        String encodedToken = Base64.getEncoder().encodeToString(basicToken.getBytes());

        RequestSpecification request = RestAssured.given();
        request.header("Authorization", "Basic " + encodedToken)
                .header("Content-Type", "application/json");
        JSONObject requestJson = new JSONObject();
        requestJson.put("grant_type", "password")
                .put("username", login)
                .put("password", Base64.getEncoder().encodeToString(password.getBytes()));
        request.body(requestJson.toString());
        Log.info("Performing POST request to " + BASE_URL + OAUTH_URL_PART);
        return request.post(BASE_URL + OAUTH_URL_PART)
                .then().assertThat().statusCode(HttpStatus.SC_OK)
                .extract().asString();
    }

}
