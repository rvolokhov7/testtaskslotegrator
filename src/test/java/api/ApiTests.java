package api;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.RequestSpecification;
import org.apache.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class ApiTests extends ApiHelper {

    SoftAssert softAssert = new SoftAssert();
    String basicAuthToken = "front_2d6b0a8391742f5d789d7d915755e09e";

    @Test(description = "Test get guest token")
    public void testGetGuestToken() throws JSONException {

        Log.step("Get guest token");
        String responseAsString = getGuestToken(basicAuthToken);
        JSONObject guestJson = new JSONObject(responseAsString);
        Assert.assertTrue(guestJson.has("access_token"), "Response does not contain access token");

    }

    @Test(description = "Test new player registration")
    public void testRegistrationOfNewPlayer() throws JSONException {

        Log.step("Get guest token");
        JSONObject guestJson = new JSONObject(getGuestToken(basicAuthToken));
        String guestToken = guestJson.getString("access_token");
        String postFix = ApiHelper.generatePostfix();

        Log.step("new player registration");
        String userName = "testname_" + postFix;
        String password = "pass_" + postFix;
        String email = "email_" + postFix + "@example.com";
        String name = "John";
        String surname = "Doe";
        String responseAsString = newPlayerRegistration(guestToken, userName, password, email, name, surname);

        Log.info("Checking retrieved data...");
        JSONObject newUserJson = new JSONObject(responseAsString);
        softAssert.assertFalse(newUserJson.isNull("id"), "id is null");
        softAssert.assertTrue(newUserJson.isNull("country_id"), "country_id is null");
        softAssert.assertTrue(newUserJson.isNull("timezone_id"), "timezone_id is null");
        softAssert.assertEquals(newUserJson.getString("username"), userName, "wrong username field value");
        softAssert.assertEquals(newUserJson.getString("email"), email, "wrong email");
        softAssert.assertEquals(newUserJson.getString("name"), name, "wrong name");
        softAssert.assertEquals(newUserJson.getString("surname"), surname, "wrong surname");
        softAssert.assertTrue(newUserJson.isNull("gender"), "gender is null");
        softAssert.assertTrue(newUserJson.isNull("phone_number"), "phone_number is null");
        softAssert.assertTrue(newUserJson.isNull("birthdate"), "birthdate is null");
        softAssert.assertTrue(newUserJson.getBoolean("bonuses_allowed"), "bonuses_allowed field value");
        softAssert.assertFalse(newUserJson.getBoolean("is_verified"), "wring bonuses_allowed field value");

        softAssert.assertAll();

    }

    @Test(description = "Test new player authorization")
    public void testNewPlayerAuthorization() throws JSONException {

        Log.step("Get guest token");
        String basicAuthToken = "front_2d6b0a8391742f5d789d7d915755e09e";
        JSONObject guestJson = new JSONObject(getGuestToken(basicAuthToken));
        String guestToken = guestJson.getString("access_token");
        String postFix = ApiHelper.generatePostfix();

        Log.step("new player registration");
        String userName = "testname_" + postFix;
        String password = "pass_" + postFix;
        String email = "email_" + postFix + "@example.com";
        String name = "John";
        String surname = "Doe";
        String s = newPlayerRegistration(guestToken, userName, password, email, name, surname);

        Log.step("new player authorization");
        String responseAsString = authorization(basicAuthToken, userName, password);

        JSONObject userJson = new JSONObject(responseAsString);
        Assert.assertTrue(userJson.has("access_token"), "Response does not contain access token");

    }

    @Test(description = "Test requesting user data")
    public void testRequestingUserData() throws JSONException {

        Log.step("Get guest token");
        String basicAuthToken = "front_2d6b0a8391742f5d789d7d915755e09e";
        JSONObject guestJson = new JSONObject(getGuestToken(basicAuthToken));
        String guestToken = guestJson.getString("access_token");

        Log.step("new player registration");
        String postFix = ApiHelper.generatePostfix();
        String userName = "name_" + postFix;
        String password = "pass_" + postFix;
        String email = "email_" + postFix + "@example.com";
        String name = "John";
        String surname = "Doe";
        String responseAsString = newPlayerRegistration(guestToken, userName, password, email, name, surname);
        JSONObject newUserJson = new JSONObject(responseAsString);
        int userId = newUserJson.getInt("id");

        String url = BASE_URL + String.format(SPECIFIC_PLAYER_URL_PART, userId);

        Log.step("new player authorization");
        responseAsString = authorization(basicAuthToken, userName, password);

        JSONObject userJson = new JSONObject(responseAsString);
        Assert.assertTrue(userJson.has("access_token"), "Response does not contain access token");
        String userToken = userJson.getString("access_token");

        Log.step("requesting user data");
        RequestSpecification request = RestAssured.given();
        Log.info("Performing GET request to " + url);
        responseAsString = request.header("Authorization", "Bearer " + userToken)
                .header("Content-Type", "application/json")
                .get(url).then().assertThat()
                .statusCode(HttpStatus.SC_OK)
                .extract().asString();

        Log.info("Checking retrieved data...");
        userJson = new JSONObject(responseAsString);
        softAssert.assertEquals(userJson.getInt("id"), userId, "id is null");
        softAssert.assertEquals(userJson.getString("username"), userName, "wrong username field value");
        softAssert.assertEquals(userJson.getString("email"), email, "wrong email");
        softAssert.assertEquals(userJson.getString("name"), name, "wrong name");
        softAssert.assertEquals(userJson.getString("surname"), surname, "wrong surname");
        softAssert.assertTrue(userJson.has("country_id"), "country_id field is absent");
        softAssert.assertTrue(userJson.has("timezone_id"), "timezone_id field is absent");
        softAssert.assertTrue(userJson.has("gender"), "gender field is absent");
        softAssert.assertTrue(userJson.has("phone_number"), "phone_number field is absent");
        softAssert.assertTrue(userJson.has("birthdate"), "birthdate field is absent");
        softAssert.assertTrue(userJson.has("bonuses_allowed"), "wrong bonuses_allowed field value");
        softAssert.assertTrue(userJson.has("is_verified"), "wrong is_verified field value");

        softAssert.assertAll();
    }

    @Test(description = "Test request other user data")
    public void testRequestingOtherUserData() throws JSONException {

        Log.step("Get guest token");
        String basicAuthToken = "front_2d6b0a8391742f5d789d7d915755e09e";
        JSONObject guestJson = new JSONObject(getGuestToken(basicAuthToken));
        String guestToken = guestJson.getString("access_token");
        String url = BASE_URL + String.format(SPECIFIC_PLAYER_URL_PART, 1);

        Log.step("new player registration");
        String postFix = ApiHelper.generatePostfix();
        String userName = "name_" + postFix;
        String password = "pass_" + postFix;
        String email = "email_" + postFix + "@example.com";
        String name = "John";
        String surname = "Doe";
        newPlayerRegistration(guestToken, userName, password, email, name, surname);

        Log.step("new player authorization");
        String responseAsString = authorization(basicAuthToken, userName, password);

        JSONObject userJson = new JSONObject(responseAsString);
        Assert.assertTrue(userJson.has("access_token"), "Response does not contain access token");
        String userToken = userJson.getString("access_token");

        Log.step("requesting other user data");
        RequestSpecification request = RestAssured.given();
        request.header("Authorization", "Bearer " + userToken)
                .header("Content-Type", "application/json");
        Log.info("Performing GET request to " + url);
        request.get(url).then().assertThat()
                .statusCode(HttpStatus.SC_NOT_FOUND)
                .extract().asString();
    }

}