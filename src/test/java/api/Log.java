package api;

public class Log {

    private static int step = 1;

    public static void nullifySteps() {
        step = 1;
        System.out.print("\n");
    }

    public static void info(String s) {
        System.out.println("INFO: " + s);
    }

    public static void step(String s) {
        System.out.println("Step " + step + ": " + s);
        step++;
    }

}
